import React from 'react';
import Logo from '../images/SpaceX-Logo.svg'


function toggle() {
    const menuToggle = document.querySelector('#menu-toggle');
    const mobileNavContainer =document.querySelector('#mobile-nav');

        menuToggle.classList.toggle('main-menu-icon-active');
        mobileNavContainer.classList.toggle('mobile-nav-active');
}


function Menu() {

    return (
        <header className='main-menu'>
            <img className='main-menu__logo' src={Logo}></img>
            <nav className='main-menu-nav'>
                <ul className='main-menu-nav-list'>
                    <li><a className='main-menu-nav-list__link' href="#">Главная</a></li>
                    <li><a className='main-menu-nav-list__link' href="#">Технология</a></li>
                    <li><a className='main-menu-nav-list__link' href="#">График полетов</a></li>
                    <li><a className='main-menu-nav-list__link' href="#">Гарантии</a></li>
                    <li><a className='main-menu-nav-list__link' href="#">О компании</a></li>
                    <li><a className='main-menu-nav-list__link' href="#">Контакты</a></li>
                </ul>
            </nav>
            <div id='menu-toggle' className='main-menu-icon' onClick={toggle}>
                <div className='main-menu-icon-line'></div>
            </div>

            <div id='mobile-nav' className='mobile-nav'>
                <div className='mobile-nav__title'>навигация </div>
                <nav className='mobile-nav-bar'>
                <ul className='mobile-nav-bar-list'>
                    <li><a className='mobile-nav-bar-list__link' href="#">Главная</a></li>
                    <li><a className='mobile-nav-bar-list__link' href="#">Технология</a></li>
                    <li><a className='mobile-nav-bar-list__link' href="#">График полетов</a></li>
                    <li><a className='mobile-nav-bar-list__link' href="#">Гарантии</a></li>
                    <li><a className='mobile-nav-bar-list__link' href="#">О компании</a></li>
                    <li><a className='mobile-nav-bar-list__link' href="#">Контакты</a></li>
                </ul>
            </nav>
            </div>

        </header>
    );
}

export default Menu;
