import React from 'react';
import Mars from '../images/mars.svg'
import Rocket from '../images/space-rocket.svg'
import $ from 'jquery'
import { setInterval } from 'timers';
import { timeout } from 'q';

function rocketStart() {

    let rock = document.getElementById("main-content-planet__rocket").style;
    let speed = 60;

    function timeIT() {
        rock.top = speed + "%";
        speed = speed - 1;
    }
    //setInterval(timeIT,30);
    let timerId = setInterval(timeIT,12);
    //alert("Поздравляем запуск произошел отлично! Это значит мы справились и вы возьмете меня на работу)")
    setTimeout(()=>{clearInterval(timerId);alert("Поздравляем запуск произошел отлично! Это значит мы справились и вы возьмете меня на работу)")},2000);
}


function Content() {
    return (
        <div className='main-content'>
            <div className='main-content-adventure'>
                <div className='main-content-adventure-text'>
                    <div className='main-content-adventure-text__title'>ПУТЕШЕСТВИЕ</div>
                    <div className='main-content-adventure-text__subtitle'>на красную планету</div>
                </div>
                <a href='#' className='main-content-adventure__button' onClick={rocketStart}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Начать
                </a>
            </div>
            <div className='main-content-planet'>
                <img className='main-content-planet__mars' src={Mars}></img>
                <img id='main-content-planet__rocket' src={Rocket}></img>
            </div>
            <div className='main-content-statistics'>
                <div className='main-content-statistics-box1'>
                    <div className='main-content-statistics-box1-block1'>
                        <div className='main-content-statistics-box1-block__title'>МЫ</div>
                        <div className='main-content-statistics-box1-block-text'>
                            <div className='main-content-statistics-box1-block-text__title'>1</div>
                            <div className='main-content-statistics-box1-block-text__subtitle'>на рынке</div>
                        </div>
                    </div>
                    <div className='main-content-statistics-box1-block2'>
                        <div className='main-content-statistics-box1-block__title'>гарантируем</div>
                        <div className='main-content-statistics-box1-block-text'>
                            <div className='main-content-statistics-box1-block-text__title'>50%</div>
                            <div className='main-content-statistics-box1-block-text__subtitle'>безопастность</div>
                        </div>
                    </div>
                </div>
                <div className='main-content-statistics-box2'>
                    <div className='main-content-statistics-box2-block3'>
                        <div className='main-content-statistics-box2-block__title'>календарик за</div>
                        <div className='main-content-statistics-box2-block-text'>
                            <div className='main-content-statistics-box2-block-text__title'>2001</div>
                            <div className='main-content-statistics-box2-block-text__subtitle'>в подарок</div>
                        </div>
                    </div>
                    <div className='main-content-statistics-box2-block4'>
                        <div className='main-content-statistics-box2-block__title'>путишествие</div>
                        <div className='main-content-statistics-box2-block-text'>
                            <div className='main-content-statistics-box2-block-text__title'>597</div>
                            <div className='main-content-statistics-box2-block-text__subtitle'>дней</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Content;
