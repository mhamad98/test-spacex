import React from 'react';
import Menu from './Components/Menu'
import Content from './Components/Content'

function App() {

  return (
    <div className='main'>
      <Menu />
      <Content />
    </div>
  );
}

export default App;
